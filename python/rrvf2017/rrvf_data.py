'''
Created on Jan 16, 2018

@author: bo
'''
import numpy as np 
import pandas as pd 
from IPython.core.display import display
import cPickle as pickle

class IndexIterator():

    def __init__(self, index):
        self.index = index
        self.N = len(self.index)
        self.shuffle()
        self.pos = 0

    def shuffle(self):
        np.random.shuffle(self.index)
        
    def next(self):
        if self.pos >= self.N:
            self.pos = 0
            self.shuffle()
        r = self.index[self.pos]
        self.pos += 1
        return r


TEST_START_DATE_IDX = 478    


class DataGen():

    def __init__(self, lag_X, ondate_X, rawy, low, high, lookback1=28, lookforward_offset=0,
                 lookforward=39, batchsize=4096, y_scale=None, indexes=None,excluded_days=None,
                 valid_split=0.0, valid_type='time_rand', valid_time_period=None,exclude_hard=False):
        assert valid_split >= 0 and valid_split < 1
        assert low >= lookback1
        assert lookforward > lookforward_offset
        self.lookback1 = lookback1
        self.lookforward = lookforward
        self.lookforward_offset = lookforward_offset 
        self.batchsize = batchsize
        self.y_scale=y_scale
        a, b = rawy.shape
        assert high + lookforward <= b 
        assert len(lag_X) == len(rawy)
        assert lag_X.shape[:2] == ondate_X.shape[:2]
        self._shape = (a, high - low)
        self.lag_X = lag_X
        self.ondate_X = ondate_X
        self.mask = np.ones_like(rawy)
        self.mask[:, 478:] = 0
        self.rawy = rawy
        self.low = low
        self.high = high
        self.idx_to_cord=self.idx_to_cord1
        N = a * (high - low)
        self.N = N
        if valid_split == 0:
            self.idx_to_cord=self.idx_to_cord2
            train_idx=np.array([(j,k) for k in range(478) for j in  range(829)]) 
            train_idx=np.array([u for u in train_idx if u[1]>=self.low and u[1] <self.high])
            print 'train #', train_idx.shape 
            train_idx=np.array([u for u in train_idx if not np.isnan(self.rawy[(u[0],u[1]+lookforward_offset)]) ])
            print 'train #', train_idx.shape 
            raw_train_idx=train_idx
            self.raw_train_iterator = IndexIterator(raw_train_idx)
            
            if exclude_hard:
                bad_ins=pickle.load(open('bad_instance.pkl'))
                train_idx=np.array([u for u in train_idx if not (u[0],u[1]+lookforward_offset) in bad_ins])
            print 'train/valid', train_idx.shape

            self.train_iterator = IndexIterator(train_idx)
            print "train has {} records out of {} records".format(self.train_iterator.N, np.prod(self._shape))
            print "no valid set"
            self.valid_iterator = None
        else:
            if valid_type == 'rand':
                if indexes is None:
                    indexes = np.array(range(self.N))
                    np.random.shuffle(indexes)
                train_N = int(1.0 * (1 - valid_split) * N)
                self.train_iterator = IndexIterator(indexes[:train_N])
                self.valid_iterator = IndexIterator(indexes[train_N:])
                print "randomly split"
                print "train has {} records out of {} records".format(self.train_iterator.N, np.prod(self._shape))
                print "valid has {} records out of {} records".format(self.valid_iterator.N, np.prod(self._shape))
            elif valid_type == 'time_rand':
                self.idx_to_cord=self.idx_to_cord2
                if indexes is None:
                    indexes = np.array(range(self.N))
                    indexes=np.array([[u//self._shape[1],u%self._shape[1]+self.low-1] for u in indexes])
                    indexes1=np.array(list(set(indexes[:,0])))
                    valid_N = int(1.0 * ( valid_split) * indexes1.shape[0])                                      
                    valid_idx1= np.random.choice(indexes1,valid_N,replace=False)
                    valid_idx=np.array([u for u in indexes if u[0] in valid_idx1])
                    train_idx=np.array([u for u in indexes if u[0] not in valid_idx1])
                    indexes=(train_idx,valid_idx) 
                else:
                    train_idx,valid_idx = indexes
                    train_idx=np.array([u for u in train_idx if u[1]>=self.low and u[1] <self.high])
                    valid_idx=np.array([u for u in valid_idx if u[1]>=self.low and u[1] <self.high])
                
                if excluded_days is None:
                    excluded_days=set([])

                #train_idx=np.array([u for u in train_idx if not np.isnan(self.rawy[tuple(u)]) and not u[1] in excluded_days])
                #valid_idx=np.array([u for u in valid_idx if not np.isnan(self.rawy[tuple(u)]) and not u[1] in excluded_days])
                print 'train/valid #', train_idx.shape,valid_idx.shape
                train_idx=np.array([u for u in train_idx if not np.isnan(self.rawy[(u[0],u[1]+lookforward_offset)]) ])
                valid_idx=np.array([u for u in valid_idx if not np.isnan(self.rawy[(u[0],u[1]+lookforward_offset)]) ])
                print 'train/valid #', train_idx.shape,valid_idx.shape
                raw_valid_idx=valid_idx
                raw_train_idx=train_idx

                if exclude_hard:
                    bad_ins=pickle.load(open('bad_instance.pkl'))
                    if 1:
                        train_idx=np.array([u for u in train_idx if not (u[0],u[1]+lookforward_offset) in bad_ins])
                        valid_idx=np.array([u for u in valid_idx if not (u[0],u[1]+lookforward_offset) in bad_ins])
                    else:
                        train_idx=np.array([u for u in train_idx if   (u[0],u[1]+lookforward_offset) in bad_ins])
                        valid_idx=np.array([u for u in valid_idx if   (u[0],u[1]+lookforward_offset) in bad_ins])                    
                print 'train/valid #', train_idx.shape,valid_idx.shape
                                        
                np.random.shuffle(train_idx)
                np.random.shuffle(valid_idx)
                self.train_iterator = IndexIterator(train_idx)
                self.valid_iterator = IndexIterator(valid_idx)
                self.raw_valid_iterator = IndexIterator(raw_valid_idx)
                self.raw_train_iterator = IndexIterator(raw_train_idx)
                
                print "rand split across time"
                print "train has {} records out of {} records".format(self.train_iterator.N, np.prod(self._shape))
                print "valid has {} records out of {} records".format(self.valid_iterator.N, np.prod(self._shape))
            elif valid_type == 'time_split':
                assert valid_time_period is not None 
                if indexes is None:
                    indexes = np.array(range(self.N)).reshape(self._shape).T
                shifted_valid_time_period = [u - self.low  for u in valid_time_period]
                train_idx = indexes[[u for u in range(indexes.shape[0]) if u not in shifted_valid_time_period and u < TEST_START_DATE_IDX]].reshape([-1])
                valid_idx = indexes[[u for u in shifted_valid_time_period if u < TEST_START_DATE_IDX]].reshape([-1])
                np.random.shuffle(train_idx)
                np.random.shuffle(valid_idx)
                self.train_iterator = IndexIterator(train_idx)
                self.valid_iterator = IndexIterator(valid_idx)
                print "rand split exclusive time", valid_time_period, indexes.shape 
                if np.max(valid_time_period) >= TEST_START_DATE_IDX: 
                    print "WARNING,  valid period includes test set"
                print "train has {} records out of {} records".format(self.train_iterator.N, np.prod(self._shape))
                print "valid has {} records out of {} records".format(self.valid_iterator.N, np.prod(self._shape))               
            
            else:
                raise "unknown"
            self.indexes=indexes
    @property
    def train_steps_per_epoch(self):
        return int(self.train_iterator.N / self.batchsize)

    @property
    def valid_steps_per_epoch(self):
        return int(self.valid_iterator.N / self.batchsize)    
    
    def support_valid(self):
        return self.valid_iterator is not None
    
    def idx_to_cord2(self, idxes):
        return idxes
    
    def idx_to_cord1(self, idxes):
        _, b = self._shape
        
        xy = idxes
        y = xy % b + self.low
        x = xy // b
        if x>=self.lag_X.shape[0]: x=self.lag_X.shape[0]-1
        return x, y

    def slice(self, cord_x, cord_z):
        # assert cord_z-self.lookback>=0
        lag_arr = self.lag_X[cord_x, (cord_z - self.lookback1):cord_z]  # on non zero target lstm
        ondate_arr = self.ondate_X[cord_x, cord_z + self.lookforward_offset: (cord_z + self.lookforward)]  # on non zero target fc
        target_y = self.rawy[cord_x , cord_z + self.lookforward_offset: (cord_z + self.lookforward)]  # output 
        mask_y = self.mask[cord_x , cord_z + self.lookforward_offset: (cord_z + self.lookforward)]        
        if self.y_scale is None:
            y_scale_arr=None
        else:
            y_scale_arr=self.y_scale[cord_x , cord_z + self.lookforward_offset: (cord_z + self.lookforward)]
        assert (ondate_arr.shape[0] == self.lookforward - self.lookforward_offset), ondate_arr.shape[0]
        assert (lag_arr.shape[0] == self.lookback1) , lag_arr.shape[0]
        assert (target_y.shape[0] == self.lookforward - self.lookforward_offset), target_y.shape[0]
        return   lag_arr, ondate_arr, target_y, mask_y, y_scale_arr

    def next_batch(self, iterator):
        lag_arr = []; ondate_arr = [];   target_y = []; mask_y = []; y_scales=[]
        while len(lag_arr) < self.batchsize:
            num = iterator.next()
            a, b, d, e, f = self.__getitem__(num)
            lag_arr.append(a)
            ondate_arr.append(b)
            target_y.append(d)
            mask_y.append(e)
            y_scales.append(f)
        target = np.array(target_y, copy=False).astype(np.float32, copy=False)
        X={'lagx': np.array(lag_arr, copy=False).astype(np.float32, copy=False),
                'ondatex': np.array(ondate_arr, copy=False).astype(np.float32, copy=False),
                'masky':np.array(mask_y, copy=False).astype(np.float32, copy=False)
               }
        if self.y_scale is not None:
            X['yscale']=np.array(y_scales, copy=False).astype(np.float32, copy=False)
        return X, [target, target]
    
    def next_train_batch(self):
        while 1:
            yield self.next_batch(self.train_iterator)
    
    def next_valid_batch(self):
        while 1:
            yield self.next_batch(self.valid_iterator)
       
    def __getitem__(self, num):
        crod_x, cord_z = self.idx_to_cord(num)
        return self.slice(crod_x, cord_z)

    def get_by_t(self, store, day):
        return self.slice(store, day)

    def get_by_t2(self, day):
        lag_arr = []; ondate_arr = [];  target_y = [];mask_y = []; y_scales=[]
        m, _ = self._shape
        indexes = []
        for i in range(m):
            a, b, d, e,f = self.get_by_t(i, day)
            lag_arr.append(a)
            ondate_arr.append(b)
            target_y.append(d)
            mask_y.append(e)
            indexes.append(i)
            y_scales.append(f)
        X={'lagx': np.array(lag_arr, copy=False).astype(np.float32, copy=False),
                'ondatex': np.array(ondate_arr, copy=False).astype(np.float32, copy=False),
                'masky':np.array(mask_y, copy=False).astype(np.float32, copy=False)
               }
        if self.y_scale is not None:
            X['yscale']=np.array(y_scales, copy=False).astype(np.float32, copy=False)
            
        return X, np.array(target_y).astype(np.float32), np.array(indexes), np.array(mask_y)

    def _get_datainput(self, iterator):
        lag_arr = []; ondate_arr = [];  target_y = [];mask_y = []; y_scales=[]
        indexes=[]
        for num in iterator.index:
            indexes.append(self.idx_to_cord(num))
            a, b, d, e,f = self.__getitem__(num)
            lag_arr.append(a)
            ondate_arr.append(b)
            target_y.append(d)
            mask_y.append(e)
            y_scales.append(f)
            
        target = np.array(target_y, copy=False).astype(np.float32, copy=False)
        X={'lagx': np.array(lag_arr, copy=False).astype(np.float32, copy=False),
                'ondatex': np.array(ondate_arr, copy=False).astype(np.float32, copy=False),
                'masky':np.array(mask_y, copy=False).astype(np.float32, copy=False)
               }
        if self.y_scale is not None:
            X['yscale']=np.array(y_scales, copy=False).astype(np.float32, copy=False)        
        return X, target, np.array(mask_y),np.array(indexes)
               
    def get_valid(self):
        assert self.support_valid() 
        return self._get_datainput(self.raw_valid_iterator)

    def get_train(self):
        return self._get_datainput(self.raw_train_iterator)
    
                           
def normalize(arr):
    m = np.nanmean(arr.reshape(-1, arr.shape[-1]), 0)
    print m.shape, m.min(), m.max()
    m[np.isnan(m)] = 0
    if 1:
        s = np.nanstd(arr.reshape(-1, arr.shape[-1]), 0)
        s[np.isnan(s)] = 0
        r = (arr - m) / (s + 1e-7)
        # r[r>4]=4
        # r[r<-4]=-4
    else:
        r = arr - m
    return r


def reverse_map(d):
    return {v:k for k, v in d.items()}


def encoding_dict(names):
    names = sorted(names)
    a = dict(enumerate(names))
    b = reverse_map(a)
    return b, a


def get_store_date_map():
    visits = pd.read_csv("../input/air_visit_data.csv", parse_dates=['visit_date'])
    stores = sorted(set(visits['air_store_id']))
    date_list = pd.date_range(pd.to_datetime("2016-01-01"), pd.to_datetime("2017-05-31"))
    return encoding_dict(stores)[1], encoding_dict(date_list)[1]


store_map = None
date_map = None 
rev_store_map = None
rev_date_map = None 

def idx_to_store(indexes):
    global store_map, date_map,rev_store_map,rev_date_map
    if store_map is None:
        store_map, date_map = get_store_date_map()
        rev_store_map=reverse_map(store_map)
        rev_date_map=reverse_map(date_map)
        
    return np.array([store_map[u] for u in indexes ])


def idx_to_date(indexes):
    global store_map, date_map,rev_store_map,rev_date_map
    if store_map is None:
        store_map, date_map = get_store_date_map()
        rev_store_map=reverse_map(store_map)
        rev_date_map=reverse_map(date_map)
    return np.array([str(date_map[u].date()) for u in indexes ])


def read_data(offset, fname="../input/train_test.npy.npz",with_l2=False, with_l3=False, with_l4=False, with_l5=False):
    a = np.load(fname)
    cols = a['columns']
    cols = np.array(['const_one'] + list(cols))
    arr = a['data']
    zero = np.array([np.ones_like(arr[0])])
    print zero.shape, arr.shape 
    arr = np.concatenate([zero, arr], axis=0)
    
    for i in range(7):
        cols = np.array(['weekday'+str(i)] + list(cols))
        zero = np.array([np.ones_like(arr[0])])
        t=np.array([u%7==i for u in range(zero.shape[-1])]).reshape([1,1,-1]).astype(np.float32)
        assert t.shape[-1]==517
        zero=zero*t
        arr = np.concatenate([zero, arr], axis=0)
    if offset is not None:
        rvarr= pickle.load(open('../input/reserve_info_offset{}.pkl'.format(offset)))
        cols=np.concatenate([cols, rvarr[0]])
        arr=np.concatenate([arr,rvarr[1]],axis=0)
    if with_l2:
        assert offset is not None
        a=np.load('../input/l1_prediction_offset{}.npz'.format(offset))
        acols = np.array(a['columns'])
        cols=np.concatenate([cols, acols])
        aarr = a['arr']    
        arr=np.concatenate([arr,aarr],axis=0)
    if with_l3:
        assert offset is not None and with_l2
        a=np.load('../input/l2_prediction_offset{}.npz'.format(offset))
        acols = np.array(a['columns'])
        cols=np.concatenate([cols, acols])
        aarr = a['arr']    
        arr=np.concatenate([arr,aarr],axis=0)
    if with_l4:
        assert offset is not None and with_l3
        a=np.load('../input/l3_prediction_offset{}.npz'.format(offset))
        acols = np.array(a['columns'])
        cols=np.concatenate([cols, acols])
        aarr = a['arr']    
        arr=np.concatenate([arr,aarr],axis=0)
    if with_l5:
        assert offset is not None and with_l4
        a=np.load('../input/l4_prediction_offset{}.npz'.format(offset))
        acols = np.array(a['columns'])
        cols=np.concatenate([cols, acols])
        aarr = a['arr']    
        arr=np.concatenate([arr,aarr],axis=0)
        
    arr = np.rollaxis(arr, 0, 3)
    return cols, arr


def show_data(data, columns, store, date, filters=None):
    idx_range = np.array(range(date - 6, date + 3))
    
    ab = np.squeeze(data[store, idx_range, :])
    print ab.shape
    filtered_columns = columns
    if filters is not None: 
        filtered_columns = [u for u in columns if filters(u)]
    df = pd.DataFrame(ab, columns=columns, index=idx_range)[filtered_columns]
    with pd.option_context('display.max_columns', None):
        display(df)

        
def show_data2(data, columns, days=range(475, 485)):
    lst = []
    for i in range(len(columns)):
        print i,
        lst.append(columns[i])
        if 1:
            for d in days:
                u = data[:, d, i]
                lst.append(np.round(np.isnan(u).mean(), 4))
            # break
    print len(lst)
    df = pd.DataFrame(np.array(lst).reshape([-1, len(days) + 1]) , columns=['col'] + days)
    return df


def show_features(datagen, ondate_cols, lag_cols, rawy, store, day, filter_fn=None):
    lag_arr, ondate_arr , target_y, _ = datagen.get_by_t(store, day)
    df1 = pd.DataFrame(ondate_arr, columns=ondate_cols, index=range(day, day + len(ondate_arr)))
    df1['target_y_0'] = target_y
    # display(df1)
    df2 = pd.DataFrame(lag_arr, columns=lag_cols, index=range(day - len(lag_arr), day))
    # display(df2)
    # display(df3)  
    idx = sorted(set(list(df1.index) + list(df2.index)))
    df = pd.concat([df1, df2 ], axis=1).loc[idx]
    df['raw_y_0'] = rawy[store][df.index]
    if filter_fn:
        df = df[[u for u in df.columns if filter_fn(u)]]
    with pd.option_context('display.max_columns', None):
        display(df)
        

def get_train_valid_indexes():
    return [pickle.load(open("../input/train5_idx_{}.pkl".format(u))) for  u in range(5)]

