'''
Created on Jan 16, 2018

@author: bo
'''

from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers import Dense, Activation, InputLayer, Dropout, BatchNormalization
from keras import metrics, backend, regularizers, backend as K, losses
from keras.layers import Input, CuDNNLSTM, RepeatVector, Concatenate, TimeDistributed, Lambda
from keras import Model
import keras
import tensorflow as tf
from keras.engine.topology import Layer
from keras.backend.tensorflow_backend import set_session
import numpy as np 
from sklearn.metrics import mean_squared_error
import math
import os 
import cPickle as pickle 
import pandas as pd 
import quadprog
import matplotlib.pyplot as plt 


class TransformNALayer(Layer):

    def __init__(self, n_input, use_dropout=False, **kwargs):
        super(TransformNALayer, self).__init__(**kwargs)
        self.supports_masking = True
        self.use_dropout = use_dropout
        self.n_input = n_input

    def build(self, input_shape):
        super(TransformNALayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x, training=None):

        def make_na(x):
            ind = tf.is_nan(x)
            filled_x = tf.where(ind, tf.zeros_like(x, dtype=tf.float32), x)
            ind = tf.cast(ind, tf.float32)
            filled_x = tf.cast(filled_x, tf.float32)
            return  tf.cast(tf.concat([filled_x, ind], axis=-1), tf.float32)

        def add_dropout(x):

            def f(x):
                ind = tf.random_uniform([1, self.n_input]) < tf.random_uniform([1])[0] / tf.constant(5.0)
                ind2 = tf.cast(ind, tf.float32) * tf.ones_like(x)
                newx = tf.where(ind2 > 0, tf.constant(np.nan, dtype=tf.float32) * tf.ones_like(x), x)
                return newx

            return tf.cond(tf.random_uniform([1])[0] > tf.constant(0.5), lambda: f(x), lambda : x)

        if self.use_dropout:
            return K.in_train_phase(make_na(add_dropout(x)), make_na(x), training=training)
        else:
            return  make_na(x) 

        # return make_na(x)
    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[1], input_shape[2] * 2) 

    def get_config(self):
        config = {
            'n_input': self.n_input,
            'use_dropout': self.use_dropout
        }
        base_config = super(TransformNALayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))    
    
    
def step_decay(epoch, initial_lrate=0.01,
   drop=0.5,
   epochs_drop=1.0):
   
    lrate = initial_lrate * math.pow(drop,
            math.floor((1 + epoch) / epochs_drop))
    print "set learning rate to", lrate
    return lrate


def get_NA_MSE_ERROR(mask_y):

    def NA_MSE_ERROR(y_true, y_pred):
        ind = (tf.constant(1, dtype=tf.float32) - tf.cast(tf.is_nan(y_true), tf.float32)) * mask_y
        new_y_true = tf.where(tf.is_nan(y_true), y_pred, y_true)
        d2 = K.square(y_pred - new_y_true)
        return K.sum(d2 * ind) / (K.sum(ind) + 1e-7)

    return NA_MSE_ERROR


def get_MSE_ERROR(mask_y):

    def MSE_ERROR(y_true, y_pred):
        new_y_true = tf.where(tf.is_nan(y_true), tf.zeros_like(y_true), y_true)
        d2 = K.square(y_pred - new_y_true)
        return K.sum(d2 * mask_y) / (K.sum(mask_y) + 1e-7)

    return MSE_ERROR


def get_BINARY_ERROR(mask_y):

    def BINARY_ERROR(y_true, y_pred):
        # new_y_true=tf.where(tf.is_nan(y_true),tf.zeros_like(y_true),y_true)
        new_y_true = tf.cast(y_true > 0, tf.float32)
        s = new_y_true * tf.log(y_pred + 1e-7) + (1 - new_y_true) * tf.log(1 - y_pred + 1e-7)
        return -K.sum(s * mask_y) / (K.sum(mask_y) + 1e-7)

    return BINARY_ERROR


def ZERO_ERROR(y_true, y_pred):
    return K.mean(K.constant(0, dtype=tf.float32) * y_pred)


def get_NA_POISSON_ERROR(mask_y):

    def NA_POSSION_ERROR(y_true, y_pred):
        ind = (tf.constant(1, dtype=tf.float32) - tf.cast(tf.is_nan(y_true), tf.float32)) * mask_y
        y_true = tf.where(tf.is_nan(y_true), y_pred, y_true)
        y_pred = (tf.cast(y_pred, tf.float32))
        y_true = (tf.cast(y_true, tf.float32))
        d2=y_pred - y_true*tf.log(y_pred+1e-7) + \
                          tf.lgamma(y_true+1.0)
        return K.sum(d2 * ind) / (K.sum(ind) + 1e-7)
    return NA_POSSION_ERROR


class NB(object):
    def __init__(self, mask_y, theta=None, theta_init=[0.0],
                 scale_factor=1.0, scope='nbinom_loss/',
                 debug=False, **theta_kwargs):
        
        # for numerical stability
        self.eps = 1e-7
        self.mask_y=mask_y
        self.scale_factor = scale_factor
        self.debug = debug
        self.scope = scope
        
        with tf.name_scope(self.scope):
            # a variable may be given by user or it can be created here
            if theta is None:
                theta = tf.Variable(theta_init, dtype=tf.float32,
                                    name='theta', **theta_kwargs)

            # keep a reference to the variable itself
            self.theta_variable = theta

            # to keep dispersion always non-negative
            self.theta = tf.nn.softplus(theta)
           
    def loss(self, y_true, y_pred, reduce=True):
        scale_factor = self.scale_factor
        eps = self.eps
        
        with tf.name_scope(self.scope):
            ind = (tf.constant(1, dtype=tf.float32) - tf.cast(tf.is_nan(y_true), tf.float32)) * self.mask_y
            y_true = tf.where(tf.is_nan(y_true), y_pred, y_true)
            
            y_true = (tf.cast(y_true, tf.float32))
            y_pred = (tf.cast(y_pred, tf.float32)) * scale_factor
            
            
            theta = 1.0/(self.theta+eps)

            t1 = -tf.lgamma(y_true+theta+eps) 
            t2 = tf.lgamma(theta+eps)
            t3 = tf.lgamma(y_true+1.0) 
            t4 = -(theta * (tf.log(theta+eps)))
            t5 = -(y_true * (tf.log(y_pred+eps)))
            t6 = (theta+y_true) * tf.log(theta+y_pred+eps)      

            if self.debug:
                tf.summary.histogram('t1', t1)
                tf.summary.histogram('t2', t2)
                tf.summary.histogram('t3', t3)
                tf.summary.histogram('t4', t4)
                tf.summary.histogram('t5', t5)
                tf.summary.histogram('t6', t6)

            final = t1 + t2 + t3 + t4 + t5 + t6
            
            if reduce:
                return K.sum(final * ind) / (K.sum(ind) + 1e-7)
                #final = tf.reduce_mean(final)
            
        return final

class Gompertz(object):
    def __init__(self, mask_y,  mu_init=[0.12],
                 scale_init=0.74, scope='gomeprtz_loss/',
                 debug=False, ):
        
        # for numerical stability
        self.eps = 1e-7
        self.mask_y=mask_y
        self.scope = scope
        
        with tf.name_scope(self.scope):
            self.mu = tf.Variable(mu_init, dtype=tf.float32, name='mu' )
            self.scale = tf.Variable(scale_init, dtype=tf.float32, name='scale' )
            #self.mu = tf.constant(mu_init, dtype=tf.float32, name='mu' )
            #self.scale = tf.constant(scale_init, dtype=tf.float32, name='scale' )           
    def loss(self, y_true, y_pred, reduce=True):
        eps = self.eps
        
        with tf.name_scope(self.scope):
            ind = (tf.constant(1, dtype=tf.float32) - tf.cast(tf.is_nan(y_true), tf.float32)) * self.mask_y
            y_true = tf.where(tf.is_nan(y_true), y_pred, y_true)
            
            y_true = (tf.cast(y_true, tf.float32)) - self.mu
            y_pred = (tf.cast(y_pred, tf.float32)) - self.mu
            
            y_true = y_true/(self.scale+eps)
            y_pred=y_pred/(self.scale+eps)
            
            #cexp(x)exp(-c(exp(x)-1))
            #log(c)+x-c(exp(x)-1)
            c=tf.cast(-np.log(0.5)/(tf.exp(y_pred)-1.0+eps),dtype=tf.float32)
            final= tf.log(c+eps)+y_true-c*(tf.exp(y_true)-1)
            final=-final
            if reduce:
                return K.sum(final * ind) / (K.sum(ind) + 1e-7)
            
        return final

class WeightsCallback(keras.callbacks.Callback):

    def __init__(self, obj):
        self.obj = obj

    def on_epoch_end(self, epoch, logs={}):
        if epoch == 2:
            self.obj.alpha.set_value(1.0)
            self.obj.beta.set_value(0.0)
            self.obj.gamma.set_value(0.0)
        print("epoch %s, alpha = %s, beta = %s, gamma=%s" % (epoch, K.get_value(self.obj.alpha),
                                                   K.get_value(self.obj.beta), K.get_value(self.obj.gamma)))

        
class TerminateOnBadInit(keras.callbacks.Callback):
    """Callback that terminates training when a NaN loss is encountered.
    """

    def __init__(self,threshold,lossname='loss'):
        super(TerminateOnBadInit, self).__init__()
        self.threshold=threshold
        self.lossname=lossname
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        loss = logs.get(self.lossname)
        self.model.abnormal_stop=False
        if loss is not None:
            if np.isnan(loss) or np.isinf(loss) or loss>self.threshold:
                print('epoch %d: Invalid loss, terminating training' % (epoch))
                self.model.stop_training = True
                self.model.abnormal_stop=True
                
class KerasModel:

    def __init__(self, name, n_lookforward, n_reg_lookback, ondate_input_dim, lag_input_dim,
                 use_dropout=False, seq2seq=False, max_epoch=200, use_output_exp=False, n_offset=0,
                 y_center_adj=False, model_type='lstm', 
                 params={}):
        self.params = params
        self.name = name 
        self.n_lookforward = n_lookforward
        self.n_reg_lookback = n_reg_lookback
        self.n_offset=n_offset
        self.ondate_input_dim = ondate_input_dim
        self.lag_input_dim = lag_input_dim
        self.use_dropout = use_dropout 
        self.use_output_exp=use_output_exp
        self.y_center_adj=y_center_adj
        self.seq2seq = seq2seq
        self.max_epoch = max_epoch 
        self.model_type=model_type
        self.model = None
        self.histdf = None 
        self.validgen = None 
        self.traingen = None 
        self.weights=None 
    def _build_reg_model(self):
        if self.model_type=='lstm':
            return self._build_lstm_reg_model()
        if self.model_type=='fc':
            return self._build_fc_reg_model()
        if self.model_type=='conv':
            return self._build_conv_reg_model()        
    def _build_conv_reg_model(self):
        params = self.params
        input_ondate = Input(shape=(self.n_lookforward-self.n_offset, self.ondate_input_dim), name='ondatex')
        input_ondate_trans = TransformNALayer(self.ondate_input_dim, self.use_dropout)(input_ondate)
        if params['batch_normalization']:        
            input_ondate_trans = BatchNormalization()(input_ondate_trans)
        input_x = Input(shape=(self.n_reg_lookback, self.lag_input_dim), name='lagx')
        input_x_trans = TransformNALayer(self.lag_input_dim, self.use_dropout)(input_x)

        if params['batch_normalization']:        
            input_x_trans = BatchNormalization()(input_x_trans)
        print "input_x_trans shape:", input_x_trans.shape
        encoded=input_x_trans
        for _ in range(2):
            encoded = keras.layers.Conv1D(128,kernel_size=[3],padding='same')(encoded)
            encoded = keras.layers.Activation('relu')(encoded)
            #encoded = keras.layers.Conv1D(128,kernel_size=[3],padding='same')(encoded) 
            #encoded = keras.layers.Activation('relu')(encoded)
            encoded=keras.layers.MaxPool1D([2])(encoded)
        encoded=Lambda(lambda u: K.reshape(u,[-1,7*128]))(encoded)
        print encoded.shape
        
        encoded = RepeatVector(self.n_lookforward-self.n_offset)(encoded)
        print encoded.shape
        decoded = encoded
        merged = Concatenate()([decoded, input_ondate_trans])
        if 1:
            fc1 = TimeDistributed(Dense(256, activation='relu'))(merged)
            fc1 = TimeDistributed(Dense(256, activation='relu'))(fc1)
            #fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
            if self.use_output_exp:
                fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
                fc2= Lambda(lambda u: tf.expm1(u))(fc2)
                fc2 = TimeDistributed(Dense(1))(fc2)
            else:
                fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
                
            output = Lambda(lambda u: K.squeeze(u, -1) )(fc2)
            inputs=[input_ondate, input_x]
            if self.y_center_adj:
                center_input=Input(shape=(self.n_lookforward-self.n_offset,), name='yscale')
                output=Lambda(lambda u:u[0]+u[1], name='reg')([output,center_input])    
                inputs.append(center_input)
            else:
                output=Lambda(lambda u:u, name='reg')(output)
            return inputs, output 
        
    def _build_fc_reg_model(self):
        params = self.params
        input_ondate = Input(shape=(self.n_lookforward-self.n_offset, self.ondate_input_dim), name='ondatex')
        input_ondate_trans = TransformNALayer(self.ondate_input_dim, self.use_dropout)(input_ondate)
        if params['batch_normalization']:        
            input_ondate_trans = BatchNormalization()(input_ondate_trans)
        input_x = Input(shape=(self.n_reg_lookback, self.lag_input_dim), name='lagx')
        input_x_trans = TransformNALayer(self.lag_input_dim, self.use_dropout)(input_x)
        #input_x_trans=Lambda(lambda u: K.reshape(u,[-1,self.n_reg_lookback*self.lag_input_dim*2]))(input_x_trans)
        print "two input shape", input_x_trans.shape, input_ondate_trans.shape        
        input_x_trans=keras.layers.Reshape([1,self.n_reg_lookback*self.lag_input_dim*2])(input_x_trans)
        if params['batch_normalization']:        
            input_x_trans = BatchNormalization()(input_x_trans)
        print "two input shape", input_x_trans.shape, input_ondate_trans.shape
        merged = Concatenate()([input_x_trans, input_ondate_trans])
        if 1:
            fc1 = TimeDistributed(Dense(256, activation='relu'))(merged)
            fc1 = TimeDistributed(Dense(256, activation='relu'))(fc1)
            fc1 = TimeDistributed(Dense(256, activation='relu'))(fc1)            
            if self.use_output_exp:
                fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
                fc2= Lambda(lambda u: tf.expm1(u))(fc2)
                fc2 = TimeDistributed(Dense(1))(fc2)
            else:
                fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
                
            output = Lambda(lambda u: K.squeeze(u, -1) )(fc2)
            inputs=[input_ondate, input_x]
            if self.y_center_adj:
                center_input=Input(shape=(self.n_lookforward-self.n_offset,), name='yscale')
                output=Lambda(lambda u:u[0]+u[1], name='reg')([output,center_input])    
                inputs.append(center_input)
            else:
                output=Lambda(lambda u:u, name='reg')(output)
            return inputs, output 
  
    def _build_lstm_reg_model(self):
        params = self.params
        input_ondate = Input(shape=(self.n_lookforward-self.n_offset, self.ondate_input_dim), name='ondatex')
        input_ondate_trans = TransformNALayer(self.ondate_input_dim, self.use_dropout)(input_ondate)
        if params['batch_normalization']:        
            input_ondate_trans = BatchNormalization()(input_ondate_trans)
        input_x = Input(shape=(self.n_reg_lookback, self.lag_input_dim), name='lagx')
        input_x_trans = TransformNALayer(self.lag_input_dim, self.use_dropout)(input_x)

        if params['batch_normalization']:        
            input_x_trans = BatchNormalization()(input_x_trans)
        print input_x_trans.shape
        encoded = CuDNNLSTM(256)(input_x_trans)
        encoded = RepeatVector(self.n_lookforward-self.n_offset)(encoded)
        if self.seq2seq:
            decoded = CuDNNLSTM(256, return_sequences=True)(encoded)
        else:
            decoded = encoded
        merged = Concatenate()([decoded, input_ondate_trans])
        if 1:
            fc1 = TimeDistributed(Dense(256, activation='relu'))(merged)
            fc1 = TimeDistributed(Dense(256, activation='relu'))(fc1)
            #fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
            if self.use_output_exp:
                fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
                fc2= Lambda(lambda u: tf.expm1(u))(fc2)
                fc2 = TimeDistributed(Dense(1))(fc2)
            else:
                fc2 = TimeDistributed(Dense(1, activation='relu'))(fc1)
                
            output = Lambda(lambda u: K.squeeze(u, -1) )(fc2)
            inputs=[input_ondate, input_x]
            if self.y_center_adj:
                center_input=Input(shape=(self.n_lookforward-self.n_offset,), name='yscale')
                output=Lambda(lambda u:u[0]+u[1], name='reg')([output,center_input])    
                inputs.append(center_input)
            else:
                output=Lambda(lambda u:u, name='reg')(output)
            return inputs, output 
  
    def _make_model(self):
        params = self.params
        
        mask_y = Input(shape=(self.n_lookforward-self.n_offset,), name='masky')
        inputs1, reg_output = self._build_reg_model()

        output = Lambda(lambda u: u, name='total')(reg_output)
        inputs = [mask_y] + inputs1
        outputs = [ output, reg_output]
        model = Model(inputs=inputs, outputs=outputs)

        if params['loss']=="poisson_error":
            totalloss=get_NA_POISSON_ERROR(mask_y)
        elif params['loss']=="nb_error":
            param_mu = tf.Variable(tf.random_uniform([1], 0.001, 0.01))
            nb = NB(mask_y,theta_init=tf.zeros([1, 1]))
            nbinom_loss, param_theta = nb.loss, nb.theta
            totalloss = nbinom_loss
        elif params['loss']=="gompertz_error":
            nb = Gompertz(mask_y )
            nbinom_loss = nb.loss 
            totalloss = nbinom_loss            
            self.nb=nb
        elif params['loss']=="mean_squared_error":
            totalloss=get_NA_MSE_ERROR(mask_y)
            self.totalloss=totalloss
        else:
            raise 1
            
        this_losses = {'reg':get_NA_MSE_ERROR(mask_y),
                     'total': totalloss }

        this_loss_weights = {'reg':0, 'total': 1.0 }
        optimizer=keras.optimizers.Adam(clipvalue=1.0 )
        model.compile(optimizer=optimizer, #params['optimizer'],
                      loss=this_losses,
                      loss_weights=this_loss_weights,
                      metrics={})

        self.model = model
        return self

    def fit(self, traingen, validgen=None):
        self.validgen = validgen
        self.traingen = traingen 
        params = self.params
        patience = params['patience']
        if self.model is None:
            self._make_model()
        if not os.path.exists(self.name):
            os.makedirs(self.name)
                
        modelname = self.name + '/model_adam.{epoch:03d}.h5'

        early_stopping = EarlyStopping(monitor='val_reg_loss', patience=patience)
        checkpoint = keras.callbacks.ModelCheckpoint(modelname, monitor='val_reg_loss',
                                                     verbose=1,
                                                     save_best_only=False,
                                                     save_weights_only=False,
                                                     mode='auto', period=1)        
        if validgen is not None:
            validation_data = validgen.next_train_batch()
            validation_steps = validgen.train_steps_per_epoch
        else:
            validation_data = traingen.next_valid_batch()
            validation_steps = traingen.valid_steps_per_epoch
        if self.model_type=='lstm':
            tob= TerminateOnBadInit(1)
        if self.model_type=='fc':
            tob= TerminateOnBadInit(1)
        if self.model_type=='conv':
            tob= TerminateOnBadInit(1)
        if   self.params['loss']=="nb_error":
            tob= TerminateOnBadInit(1, 'reg_loss')

            
        self.hists = self.model.fit_generator(traingen.next_train_batch(),
                                              steps_per_epoch=traingen.train_steps_per_epoch,
                                              validation_steps=validation_steps,
                                              epochs=self.max_epoch,
                                              validation_data=validation_data,
                                              callbacks=[early_stopping, checkpoint,tob], verbose=1)
        histdf = pd.DataFrame(self.hists.history)
        histdf['epoch'] = histdf.index
        histdf = histdf.sort_values('val_total_loss')
        histdf.to_csv(self.name + "/" + self.name + ".hist.csv")
        self.histdf = histdf
        self.save_info()
        return self

    @property
    def best_epoch(self):
        if self.histdf is None:
            hist_csv = self.name + "/" + self.name + ".hist.csv"
            if os.path.exists(hist_csv):
                self.histdf = pd.read_csv(hist_csv)
            else:
                raise Exception("Seems model has not been trained")
        best_epochs = (self.histdf.iloc[:5]['epoch']+1).tolist()
        print "best_epochs", best_epochs 
        filenames = [self.name + '/model_adam.{epoch:03d}.h5'.format(epoch=u) for u in best_epochs]
        return filenames 

    def get_weights(self):
        if self.weights is None:
            hist_csv = self.name + "/" + "info.pkl"
            if os.path.exists(hist_csv):
                self.histdf,self.weights = pickle.load(open(hist_csv))
            else:
                raise Exception("Seems model has not been trained")
        return self.weights
    
    def eval_valid(self, b_show=False):
        filenames = self.best_epoch
        if self.validgen is not None: 
            X, y, mask, valididx = self.validgen.get_train()
        else:
            X, y, mask,valididx = self.traingen.get_valid()

        mask = (np.array(mask) > 0)
        loss,ypred=eval_model(filenames, X, y, mask,self.get_weights(), b_show=b_show,return_pred=True)
        return loss, y,ypred,valididx

    def eval_train(self, b_show=False):
        filenames = self.best_epoch
        X, y, mask, valididx = self.traingen.get_train()
        mask = (np.array(mask) > 0)
        loss,ypred=eval_model(filenames, X, y, mask,self.get_weights(), b_show=b_show,return_pred=True)
        return loss, y,ypred,valididx
    
    def eval_datagen_train(self, datagen, b_show=False):
        filenames = self.best_epoch
        X, y, mask, valididx = datagen.get_train()
        mask = (np.array(mask) > 0)
        loss,ypred=eval_model(filenames, X, y, mask,self.get_weights(), b_show=b_show,return_pred=True)
        return loss, y,ypred,valididx
    
    def save_info(self):
        fname = self.name + "/" + "info.pkl"
        if self.weights is None:
            self.eval_weights()
        pickle.dump([self.histdf, self.weights], open(fname, 'wb'))

    def eval_weights(self, b_show=False):
        filenames = self.best_epoch
        if self.validgen is not None: 
            X, y, mask,_ = self.validgen.get_train()
        else:
            X, y, mask,_ = self.traingen.get_valid()
        aa = make_pred(filenames, X)
        weights = make_weights(aa, y, mask)
        self.weights = weights 
        return weights 
    
    def predict(self, day):
        X, y, w, mask = self.traingen.get_by_t2(day)
        filenames = self.best_epoch
        aa = make_pred(filenames, X)
        aa = np.array(aa)
        yy_pred = np.sum(aa * self.get_weights().reshape([-1, 1, 1]), 0)
        print yy_pred.shape, np.mean(yy_pred), w.shape
        return yy_pred, w, y, mask 
    
    def predict_train(self):
        X, y, mask,w = self.traingen.get_train()
        filenames = self.best_epoch
        aa = make_pred(filenames, X)
        aa = np.array(aa)
        yy_pred = np.sum(aa * self.get_weights().reshape([-1, 1, 1]), 0)
        print yy_pred.shape, np.mean(yy_pred), w.shape
        return yy_pred, w, y, mask, X
    
    def make_submission(self):
        from rrvf_data import idx_to_date,idx_to_store
        days=idx_to_date(range(478,478+self.n_lookforward))    
        yy_pred, w, _, _=a.predict(478)    
        stores=idx_to_store(w)
        def f(i):
            u=yy_pred[:,i]
            df=pd.DataFrame({'air_store_id':stores, 'pred':u})
            df['visit_date']=days[i]
            return df

        preddf=pd.concat([f(i) for i in range(len(days))])
        preddf['pred_visitors']=np.expm1(preddf['pred'])
        sample=pd.read_csv("../input/sample_submission.csv",index_col=0)
        sample['air_store_id']=sample.index.map(lambda u: "_".join(u.split("_")[:2]))
        sample['visit_date']=sample.index.map(lambda u:  u.split("_")[2])
        #display(sample.head());     print sample.dtypes
        preddf=pd.concat([f(i) for i in range(len(days))])
        preddf['pred_visitors']=np.expm1(preddf['pred'])
        #display(preddf.head());    print preddf.dtypes

        tmpdf=pd.merge(sample,preddf, on = ['air_store_id','visit_date'],how='left')
        tmpdf=tmpdf[['pred_visitors']]
        print "nan percent", np.mean(~tmpdf.isnull())
        tmpdf=tmpdf.fillna(0)
        tmpdf.to_csv(self.name+".csv.gz",compression='gzip', float_format='%.4f')

    
def load_model(name):
    amodel = keras.models.load_model(name, custom_objects={'TransformNALayer':TransformNALayer,
                                                      'MSE_ERROR':ZERO_ERROR,
                                                      'NA_MSE_ERROR':ZERO_ERROR,
                                                      'BINARY_ERROR':ZERO_ERROR,
                                                      'NA_POSSION_ERROR': ZERO_ERROR,
                                                      'loss': ZERO_ERROR,
                                                       'tf':tf                                                        
                                                     })
    return amodel


def quadprog_solve_qp(P, q, G=None, h=None, A=None, b=None):
    qp_G = .5 * (P + P.T)  # make sure P is symmetric
    qp_a = -q
    if G is None:
        G = np.zeros([1, P.shape[0]], dtype=np.float)
        h = np.zeros([1], dtype=np.float)
    if A is not None:
        qp_C = -np.vstack([A, G]).T
        qp_b = -np.hstack([b, h])
        meq = A.shape[0]
    else:  # no equality constraint
        qp_C = -G.T
        qp_b = -h
        meq = 0
    return quadprog.solve_qp(qp_G, qp_a, qp_C, qp_b, meq)


def make_pred(fnames, X):
    preds = []
    for fname in fnames:
        model = load_model(fname)
        yy_pred1 = model.predict(X, 4096)[0]
        preds.append(yy_pred1)
    return preds


def eval_model(fnames, X, y_true, mask, weights=None, b_show=False, return_pred=False):
    preds = make_pred(fnames, X)
    preds =np.sum( np.array(preds)*weights.reshape([-1,1,1]),0)
    loss=MSE(y_true, preds, mask, b_show=b_show)
    if return_pred:
        return loss,preds
    else:
        return loss


def MSE(y_true, y_pred, mask, b_show=False):
    mask=mask>0
    y_true = y_true.copy()
    if b_show: 
        plt.hist(y_pred.reshape(-1), bins=200);plt.show()
    lst = []
    pp = []
    for i in range(y_true.shape[1]):
        ind = ~np.isnan(y_true[:, i])
        if b_show: 
            plt.hist(y_pred[:, i][ind].reshape(-1), bins=200);plt.show()
        m = mask[:, i][ind]
        if len(y_true[:, i][ind][m]) < 10: continue
        aa = mean_squared_error(y_true[:, i][ind][m], y_pred[:, i][ind][m])
        pp.append((i + 1, aa))
        lst.append(aa)
    print np.mean(lst[:6]), np.mean(lst[:])
    for u in pp:
        print u     
    return np.mean(lst)

def make_weights(preds, y_true, mask):
    y_true_nonan = y_true.copy()
    y_true_nonan[np.isnan(y_true_nonan)] = 0 
    d = [(u - y_true_nonan)  for u in preds]
    mask = (mask>0) & (~np.isnan(y_true))
    xx = np.array([u[mask].reshape(-1) for u in d], dtype=np.float64)
    P = np.matmul(xx, xx.T) / xx.shape[1]
    G = np.diag(-np.ones([xx.shape[0]], dtype=np.float))
    h = np.zeros(shape=[xx.shape[0]], dtype=float)
    A = np.ones(shape=[1, xx.shape[0]], dtype=float)
    b = np.array([1], dtype=float)
    q = np.zeros(shape=[xx.shape[0]], dtype=float)
    # print P.shape,G.shape,h.shape,q.shape,A.shape,b.shape
    weights, optval = quadprog_solve_qp(2 * P, q, G, h, A, b)[:2]
    print 'individual loss', np.diag(P)
    print weights, optval
    return weights    
